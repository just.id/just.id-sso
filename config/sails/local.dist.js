/**
 * Local environment settings
 *
 * Use this file to specify configuration settings for use while developing
 * the app on your personal system.
 *
 * For more information, check out:
 * https://sailsjs.com/docs/concepts/configuration/the-local-js-file
 */

 module.exports = {

  port: 2337,

  custom: {

    baseUrl: 'http://just.id',

    platformCopyrightYear: '2021',

    passwordResetTokenTTL: 24*60*60*1000,// 24 hours
    emailProofTokenTTL:    24*60*60*1000,// 24 hours

    // The sender that all outgoing emails will appear to come from.
    fromEmailAddress: 'no.reply@just.id',
    fromName: 'Just.ID Team',

    // Email address for receiving support messages & other correspondences.
    // > If you're using the default privacy policy, this will be referenced
    // > as the contact email of your "data protection officer" for the purpose
    // > of compliance with regulations such as GDPR.
    internalEmailAddress: 'support@just.id',

    // Whether to require proof of email address ownership any time a new user
    // signs up, or when an existing user attempts to change their email address.
    verifyEmailAddresses: true,

    // <hostname>: <api base url>
    serviceBaseUrl: {
      'bitdocker': 'https://bitdocker.io',
    },


    // Credentials

    // Base64 encoded 256-bit (32-byte) secret for signing JWT tokens
    // jwtSecret: '64VfX54Dnmc+DnVnMTYi44xdarOlN5JwyvQ30Q9emdc=',

    jwtPrivKey: `-----BEGIN ENCRYPTED PRIVATE KEY-----
xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
-----END ENCRYPTED PRIVATE KEY-----`,

    jwtPrivKeyPassphrase: 'SECRET',

    namespace: {
      user: 'ad698c43-6ad6-4129-8a60-6092f83c97d5',
    },

    mailgunDomain: 'send.just.id',
    mailgunApiKey: 'KEY',
  },

  security: {
    cors: {
      allRoutes: false,
      allowOrigins: 'https',
      allowCredentials: false,
      allowRequestHeaders: '*',
    },
    csrf: true,
  },

  datastores: {
    default: {
      adapter: 'sails-mongo',
      url: 'mongodb://mongo:27017/just-id',
    },
  },

  models: {
    dataEncryptionKeys: {
      default: 'EXAMPLE=====CixP9JusYofKKouW0RNdRYVfzOrML8o='
    },
  },

  session: {
    secret: 'SESSION',

    adapter: '@sailshq/connect-redis',
    url: 'redis://redis:6379/0',

    cookie: {
      secure: true,
      maxAge: 30 * 24 * 60 * 60 * 1000,  // 1 month
    },

  },

  sockets: {
    adapater: '@sailshq/socket.io-redis',
    url: 'redis://redis:6379/1',
    onlyAllowOrigins: [
      'https://just.id',
    ],
  },


};
