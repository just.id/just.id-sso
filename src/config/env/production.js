/**
 * Production environment settings
 * (sails.config.*)
 *
 * What you see below is a quick outline of the built-in settings you need
 * to configure your Sails app for production.  The configuration in this file
 * is only used in your production environment, i.e. when you lift your app using:
 *
 * ```
 * NODE_ENV=production node app
 * ```
 *
 * > If you're using git as a version control solution for your Sails app,
 * > this file WILL BE COMMITTED to your repository by default, unless you add
 * > it to your .gitignore file.  If your repository will be publicly viewable,
 * > don't add private/sensitive data (like API secrets / db passwords) to this file!
 *
 * For more best practices and tips, see:
 * https://sailsjs.com/docs/concepts/deployment
 */

module.exports = {

  // Any configuration settings may be overridden below, whether it's built-in Sails
  // options or custom configuration specifically for your app (e.g. Stripe, Sendgrid, etc.)

  port: 1337,

  log: {
    level: 'warn'
  },

  custom: {

    baseUrl: 'https://just.id',

    platformCopyrightYear: '2021',

    passwordResetTokenTTL: 24*60*60*1000,// 24 hours
    emailProofTokenTTL:    24*60*60*1000,// 24 hours

    // The sender that all outgoing emails will appear to come from.
    fromEmailAddress: 'no.reply@just.id',
    fromName: 'Just.iD Team',

    // Email address for receiving support messages & other correspondences.
    // > If you're using the default privacy policy, this will be referenced
    // > as the contact email of your "data protection officer" for the purpose
    // > of compliance with regulations such as GDPR.
    internalEmailAddress: 'support@just.id',

    // Whether to require proof of email address ownership any time a new user
    // signs up, or when an existing user attempts to change their email address.
    verifyEmailAddresses: true,

  },

  models: {
    migrate: 'safe',
  },

  session: {
    cookie: {
      secure: true,
      maxAge: 12 * 60 * 60 * 1000,  // 12 hours
    },

  },

  http: {
    cache: 3 * 24 * 60 * 60 * 1000, // 3 Days
    trustProxy: true,
  },

  blueprints: {
    shortcuts: false,
  },

};
