const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

passport.serializeUser((user, cb) => {
  cb(null, user.id);
});

passport.deserializeUser( (id, cb) => {
  User.findOne({id}).exec( (err, user) => {

    if (err){
      console.log(err.message);
      return cb('serverError', null);
    }
    if (!user){
      return cb('badCombo', null);
    } else {
      return cb(null, user);
    }

  });
});

passport.use(new LocalStrategy({
  usernameField: 'emailAddress',
  passwordField: 'password'
}, (emailAddress, password, cb) => {

  if (!emailAddress || !password){
    return cb(null, false, {message: 'Invalid email address or password.'});
  }

  User.findOne({emailAddress: emailAddress.toLowerCase()}).exec( async (err, userRecord) => {
    if (err){
      console.log(err.message);
      return cb(err, false, {message: 'serverError'});
    }

    if (!userRecord){
      return cb(null, false, {message: 'Invalid email address or password.'});
    }

    // If the password doesn't match, then also exit thru "badCombo".
    await sails.helpers.passwords.checkPassword(password, userRecord.password)
    .intercept('incorrect', () => {
      return cb(null, false, {message: 'Invalid email address or password.'});
    });

    // Redact sensitive infomation
    userRecord.password = '';
    userRecord.stripeCustomerId = '';

    return cb(null, userRecord, { message: 'Login Succesful'});

  });

}));
