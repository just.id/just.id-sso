module.exports = {


  friendlyName: 'Logout',


  description: 'Logout passport.',


  inputs: {

  },


  exits: {

  },


  fn: async function (inputs) {

    var req = this.req;
    var res = this.res;

    // Clear the `userId` property from this session.
    delete req.session.userId;

    // Broadcast a message that we can display in other open tabs.
    if (sails.hooks.sockets) {
      await sails.helpers.broadcastSessionChange(req);
    }

    // await sails.helpers.logoutUserGlobal(this.req.me.id);

    // Then finish up, sending an appropriate response.
    // > Under the covers, this persists the now-logged-out session back
    // > to the underlying session store.
    if (!req.wantsJSON) {
      return res.redirect('/login');
    }

  }


};
