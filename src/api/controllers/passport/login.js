module.exports = {


  friendlyName: 'Login',


  description: 'Login passport.',


  inputs: {

    emailAddress: {
      description: 'The email to try in this attempt, e.g. "irl@example.com".',
      type: 'string',
      required: true
    },

    password: {
      description: 'The unencrypted password to try in this attempt, e.g. "passwordlol".',
      type: 'string',
      required: true
    },

    rememberMe: {
      description: 'Whether to extend the lifetime of the user\'s session.',
      extendedDescription:
`Note that this is NOT SUPPORTED when using virtual requests (e.g. sending
requests over WebSockets instead of HTTP).`,
      type: 'boolean'
    },

    sso: {
      description: 'Is this a SSO request?',
      type: 'boolean',
      required: true,
    },

  },


  exits: {

    success: {
    },

    redirect: {
      responseType: 'redirect'
    },

    badCombo: {
      description: `The provided email and password combination does not
      match any user in the database.`,
      responseType: 'unauthorized',
    },

    badRequest: {
      responseType: 'badRequest',
      description: 'Invalid login parameters.',
    },

    serverError: {
      responseType: 'serverError',
      description: 'An unexpected error happened.',
    },

  },


  fn: async function (inputs) {

    var passport = require('passport');

    var req = this.req;
    var res = this.res;

    console.log('Passport login called.');

    let resRecord = await new Promise( (resolve) => {
      passport.authenticate('local', async (err, userRecord, info) => {
        if( err || !userRecord ) {
          resolve(false);
        }

        User.updateOne({
          id: userRecord.id,
        })
        .set({
          lastLoginIP: req.ip,
        })
        .exec( (err) => {
          if (err){ sails.log.error('Error updating user login IP.'); }
        });

        // If "Remember Me" was enabled, then keep the session alive for
        // a longer amount of time.  (This causes an updated "Set Cookie"
        // response header to be sent as the result of this request -- thus
        // we must be dealing with a traditional HTTP request in order for
        // this to work.)
        if (req.param('rememberMe')) {
          if (req.isSocket) {
            sails.log.warn(
              'Received `rememberMe: true` from a virtual request, but it was ignored\n'+
              'because a browser\'s session cookie cannot be reset over sockets.\n'+
              'Please use a traditional HTTP request instead.'
            );
          } else {
            req.session.cookie.maxAge = sails.config.custom.rememberMeCookieMaxAge;
          }
        }//ﬁ


        // Modify the active session instance.
        // (This will be persisted when the response is sent.)
        req.session.userId = userRecord.id;

        // In case there was an existing session (e.g. if we allow users to go to the login page
        // when they're already logged in), broadcast a message that we can display in other open tabs.
        if (sails.hooks.sockets) {
          await sails.helpers.broadcastSessionChange(req);
        }

        sails.log.verbose('User logged in:', userRecord);

        resolve({
          message: info.message,
          userRecord
        });

      })(req, res);

    });

    if (!resRecord){ throw 'badCombo'; }

    // throw {'redirect': '/'};
    if (req.param('sso')){

      let user = resRecord.userRecord;

      let expiry = 86400 * 1000 + Date.now();

      if (req.param('rememberMe')) {
        if (!req.isSocket){
          expiry = sails.config.custom.rememberMeCookieMaxAge + Date.now();
        }
      }

      let token = await sails.helpers.generateTokenForUser.with({user, expiry});

      return {
        token: token,
      };


    } else {
      return;
    }


  }


};
