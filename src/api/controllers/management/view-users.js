module.exports = {


  friendlyName: 'View users',


  description: 'Display "Users" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/management/users'
    }

  },


  fn: async function () {

    // Respond with view.
    return {};

  }


};
