module.exports = {


  friendlyName: 'Verify',


  description: 'Verify auth token.',


  inputs: {

  },


  exits: {

    badRequest: {
      responseType: 'badRequet',
      description: 'Token not provided.'
    },

    unauthorized: {
      responseType: 'unauthorized',
      description: 'Token not valid or not found.'
    },

  },


  fn: async function (inputs) {

    var req = this.req;
    var res = this.res;

    // Get token from query
    let reqToken = req.query.token;
    if (!reqToken){ throw 'badRequest'; }

    // eslint-disable-next-line no-undef
    let token = await SsoToken.findOne({ token: reqToken }).populate('user');
    if (!token){ throw 'unauthorized'; }



  }


};
