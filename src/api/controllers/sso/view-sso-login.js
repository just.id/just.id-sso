module.exports = {


  friendlyName: 'View login',


  description: 'Display "Login" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/sso/sso-login'
    },

    redirect: {
      responseType: 'redirect',
      description: 'No return URL specified. Redirecting to normal signin page.'
    }

  },


  fn: async function () {

    let returnUrl = this.req.query.returnUrl;
    if (!returnUrl){ throw {'redirect': '/login'}; }

    // TODO: Check origin URL

    let jwtToken = {};

    if (this.req.me){
      // console.log(this.req.session);
      let expireAt = new Date(this.req.session.cookie._expires);

      // Exclude any fields corresponding with attributes that have `protect: true`.
      var sanitizedProfile = _.extend({}, this.req.me);
      await sails.helpers.redactUser(sanitizedProfile);

      // If there is still a "password" in sanitized user data, then delete it just to be safe.
      if (sanitizedProfile.password) {
        delete sanitizedProfile.password;
      }//ﬁ

      if (sanitizedProfile.stripeCustomerId){
        delete sanitizedProfile.stripeCustomerId;
      }

      jwtToken = await sails.helpers.generateTokenForUser.with({user: sanitizedProfile, expiry: expireAt.getTime()});
      // throw {'redirect': returnUrl + '?token=' + encodeURIComponent(jwtToken)};
    }

    return {
      returnUrl,
      token: encodeURIComponent(jwtToken),
      loggedIn: this.req.me? true: false,
    };

  }


};
