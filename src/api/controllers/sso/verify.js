module.exports = {


  friendlyName: 'Verify',


  description: 'Verify sso.',


  inputs: {

    token: {
      type: 'string',
      required: true,
      maxLength: 200,
    }

  },


  exits: {

    notFound: {
      responseType: 'notFound',
    },

    unauthorized: {
      responseType: 'unauthorized',
      // description: 'Token not valid or not found.'
    },

  },


  fn: async function (inputs) {

    // eslint-disable-next-line no-undef
    let record = await SsoToken.findOne({ token: inputs.token});
    if (!record){ throw 'unauthorized'; }

    let user = await User.findOne({ id: record.user });
    let jwt = require('jsonwebtoken');

    let exp = Math.floor(record.expiry / 1000 - 30);

    // Provide user information to request service. Service may create an user based on these details.
    let data = {
      uuid: user.uuid,
      emailAddress: user.emailAddress,
      emailStatus: user.emailStatus,
      fullName: user.fullName,
      isSuperAdmin: user.isSuperAdmin,
      exp,
    };

    const key = sails.config.custom.jwtPrivKey;
    const passphrase = sails.config.custom.jwtPrivKeyPassphrase;

    let token = jwt.sign(data, {key, passphrase}, {algorithm: 'ES384'});

    // Remove token in the background
    // eslint-disable-next-line no-undef
    SsoToken.destroyOne({ id: record.id}).exec( (err, resToken) => {
      if (err){
        sails.log.error(err.message);
      }

      sails.log.debug(resToken);
    });

    // All done.
    return {
      jwt: token,
    };

  }


};
