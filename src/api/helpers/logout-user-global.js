module.exports = {


  friendlyName: 'Logout user global',


  description: '',


  inputs: {

    userId: {
      type: 'string',
      required: true,
      maxLength: 32,
    }

  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function ({userId}) {
    const jwt = require('jsonwebtoken');
    const axios = require('axios');

    const userProfile = await User.findOne({ id: userId });

    if (!userProfile){
      throw 'profileNotFound';
    }

    const key = sails.config.custom.jwtPrivKey;
    const passphrase = sails.config.custom.jwtPrivKeyPassphrase;
    let jwtToken = jwt.sign({uuid: userProfile.uuid}, {key, passphrase}, {algorithm: 'ES384'});

    const params = {
      jwt: jwtToken,
    };

    // Send user profile to all services
    const apiUrl = '/api/v1/sso/logout-user';
    for (var service in sails.config.custom.serviceBaseUrl){
      const baseUrl = sails.config.custom.serviceBaseUrl[service];

      axios({
        method: 'PUT',
        url: baseUrl + apiUrl,
        data: params,
        withCredentials: false,
      })
      .then( (res) => {
        sails.log.debug('Logged out user ' + userProfile.uuid + ' at ' + baseUrl);
      })
      .catch( (err) => {
        sails.log.error('Error logging out user ' + userProfile.uuid + ' at ' + baseUrl);
        if (err.response){
          sails.log.error(err.response);
        } else if (err.request){
          sails.log.error(err.request);
        } else {
          sails.log.error(err.message);
        }
      });
    }
  }


};

