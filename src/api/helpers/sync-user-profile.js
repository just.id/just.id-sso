module.exports = {


  friendlyName: 'Sync user profile',


  description: '',


  inputs: {

    userId: {
      type: 'string',
      required: true,
      maxLength: 32,
    }

  },


  exits: {

    success: {
      description: 'All done.',
    },

    profileNotFound: {
      description: 'User profile not found.',
    }

  },


  fn: async function ({userId}) {
    const jwt = require('jsonwebtoken');
    const axios = require('axios');

    const key = sails.config.custom.jwtPrivKey;
    const passphrase = sails.config.custom.jwtPrivKeyPassphrase;

    const userProfile = await User.findOne({ id: userId });

    if (!userProfile){
      throw 'profileNotFound';
    }

    // Exclude any fields corresponding with attributes that have `protect: true`.
    var sanitizedProfile = _.extend({}, userProfile);
    sails.helpers.redactUser(sanitizedProfile);

    // If there is still a "password" in sanitized user data, then delete it just to be safe.
    if (sanitizedProfile.password) {
      delete sanitizedProfile.password;
    }//ﬁ

    if (sanitizedProfile.stripeCustomerId){
      delete sanitizedProfile.stripeCustomerId;
    }

    let jwtToken = jwt.sign(sanitizedProfile, {key, passphrase}, {algorithm: 'ES384'});

    const params = {
      jwt: jwtToken,
    };

    // Send user profile to all services
    const apiUrl = '/api/v1/sso/sync-user-profile';
    for (var service in sails.config.custom.serviceBaseUrl){
      const baseUrl = sails.config.custom.serviceBaseUrl[service];

      axios({
        method: 'PUT',
        url: baseUrl + apiUrl,
        data: params,
        withCredentials: false,
      })
      .then( (res) => {
        sails.log.debug('Synchronized user profile ' + sanitizedProfile.uuid + ' with ' + baseUrl);
      })
      .catch( (err) => {
        sails.log.error('Error synchronizing profile with ' + baseUrl);
        if (err.response){
          sails.log.error(err.response);
        } else if (err.request){
          sails.log.error(err.request);
        } else {
          sails.log.error(err.message);
        }
      });
    }
  }

};

