module.exports = {


  friendlyName: 'Mg send html email',


  description: '',


  inputs: {

    htmlMessage: {
      type: 'ref',
      description: 'HTML message to be sent via email.',
      required: true,
    },


    to: {
      description: 'The email address of the primary recipient.',
      extendedDescription: 'If this is any address ending in "@example.com", then don\'t actually deliver the message. '+
        'Instead, just log it to the console.',
      example: 'nola.thacker@example.com',
      required: true,
      isEmail: true,
    },

    toName: {
      description: 'Full name of the primary recipient.',
      example: 'Nola Thacker',
    },

    subject: {
      description: 'The subject of the email.',
      example: 'Hello there.',
      defaultsTo: ''
    },

    from: {
      description: 'An override for the default "from" email that\'s been configured.',
      example: 'anne.martin@example.com',
      isEmail: true,
    },

    fromName: {
      description: 'An override for the default "from" name.',
      example: 'Anne Martin',
    },

    bcc: {
      description: 'The email addresses of recipients secretly copied on the email.',
      example: ['jahnna.n.malcolm@example.com'],
    },

    attachments: {
      description: 'Attachments to include in the email, with the file content encoded as base64.',
      whereToGet: {
        description: 'If you have `sails-hook-uploads` installed, you can use `sails.reservoir` to get an attachment into the expected format.',
      },
      example: [
        {
          contentBytes: 'iVBORw0KGgoAA…',
          name: 'sails.png',
          type: 'image/png',
        }
      ],
      defaultsTo: [],
    },
  },


  exits: {

    success: {
      outputFriendlyName: 'Email delivery report',
      outputType: {
        loggedInsteadOfSending: 'boolean'
      }
    },

  },


  fn: async function ({htmlMessage, to, toName, bcc, subject, from, fromName, attachments}) {
    const formData = require('form-data');
    const Mailgun = require('mailgun.js');
    const mailgun = new Mailgun(formData);

    var mg = mailgun.client({
      username: 'api',
      key: sails.config.custom.mailgunApiKey
    });

    var toList = [];
    toList.push(to);

    mg.messages.create(sails.config.custom.mailgunDomain, {
      from: from? from: sails.config.custom.fromEmailAddress,
      to: toList,
      subject: subject,
      html: htmlMessage,
    })
    .then( (res) => {
      // console.log(res);
      return;
    })
    .catch( (err) => {
      console.log(err);
      return err;
    });


  }


};

