module.exports = {


  friendlyName: 'Generate user uuid',


  description: '',


  inputs: {

    string: {
      type: 'string',
      required: true,
    }

  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function ({string}) {
    const { v5: uuidv5 } = require('uuid');
    return uuidv5(string, sails.config.custom.namespace.user);
  }


};

