module.exports = {


  friendlyName: 'Generate token for user',


  description: '',


  inputs: {

    user: {
      type: 'ref',
      required: true,
    },

    expiry: {
      type: 'number',
      min: 0,
    }

  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function ({user, expiry}) {
    const tokenSet = await sails.helpers.generateToken();

    const token = tokenSet.token;
    const jwtToken = tokenSet.jwtToken;

    // Token expires in 3 minutes
    if (!expiry){ expiry = Date.now() + 3*60*1000; }

    // eslint-disable-next-line no-undef
    let newToken = await SsoToken.create({
      user: user.id,
      userUuid: user.uuid,
      expiry: expiry,
      token: token,
    }).fetch();

    sails.log.debug('Generated token:', newToken);

    return jwtToken;
  }


};
