module.exports = {


  friendlyName: 'Generate token',


  description: '',


  inputs: {

  },


  exits: {

    success: {
      description: 'All done.',
    },

  },

  sync: true,


  fn: function () {
    const Crypto = require('crypto');
    const jwt = require('jsonwebtoken');

    // if (!size){
    //   size = 32;  // 32 bytes = 128 bits
    // }

    function randomString(size) {
      return Crypto
        .randomBytes(size)
        .toString('base64')
        .slice(0, size);
    }

    let token = randomString(32);

    const key = sails.config.custom.jwtPrivKey;
    const passphrase = sails.config.custom.jwtPrivKeyPassphrase;

    let jwtToken = jwt.sign({token}, {key, passphrase}, {algorithm: 'ES384'});

    return {token, jwtToken};
  }


};

