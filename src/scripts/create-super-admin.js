module.exports = {

  friendlyName: 'Create a super admin',


  description: 'Manually create a superadmin.',


  fn: async function(){
    await User.createEach([
      {
        emailAddress: 'supa.admin@example.com',
        fullName: 'Supa Admin',
        isSuperAdmin: true,
        password: await sails.helpers.passwords.hashPassword('Somepassword'),
        uuid: await sails.helpers.generateUserUuid('superadmin' + Date.now() + Math.random()),
      },
    ]);
  }

};
