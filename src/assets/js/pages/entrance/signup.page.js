parasails.registerPage('signup', {
  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
  data: {
    // Form data
    formData: { /* … */ },

    // For tracking client-side validation errors in our form.
    // > Has property set to `true` for each invalid property in `formData`.
    formErrors: { /* … */ },

    // Form rules
    formRules: {
      fullName: {required: true},
      emailAddress: {required: true, isEmail: true, custom: (value) => {
        if (value.length > 200){ return false; }

        let testCases = ['\@just\.id$'];

        for (let i=0; i < testCases.length; i++){
          let regex = new RegExp(testCases[i]);
          if (regex.test(value)){
            return false;
          }
        }

        return true;
      }},
      // username: {required: true, custom: (value) => {
      //   if (value.length > 32){ return false; }

      //   let re = /^[a-zA-Z\.\_]+$/g;

      //   if (!re.test(value)) {
      //     return false;
      //   }

      //   let testCases = ['^[\.\_]', '[\.\_]$', '[\.\_]{2}', '^admin', '/[\.\_]admin$/', '/[\.\_]admin[\d\.\_]/', '/admin(istrator)?/'];

      //   for (let i=0; i < testCases.length; i++){
      //     let regex = new RegExp(testCases[i]);
      //     console.log(regex.test(value));
      //     if (regex.test(value)){
      //       return false;
      //     }
      //   }

      //   return true;
      // }},
      password: {required: true},
      confirmPassword: {required: true, sameAs: 'password'},
      agreed: {required: true},
    },

    // Syncing / loading state
    syncing: false,

    // Server error state
    cloudError: '',

    // Success state when form has been submitted
    cloudSuccess: false,
  },

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  beforeMount: function() {
    //…
  },
  mounted: async function() {
    //…
  },

  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  methods: {

    submittedForm: async function() {
      if(this.isEmailVerificationRequired) {
        // If email confirmation is enabled, show the success message.
        this.cloudSuccess = true;
      }
      else {
        // Otherwise, redirect to the logged-in dashboard.
        // > (Note that we re-enable the syncing state here.  This is on purpose--
        // > to make sure the spinner stays there until the page navigation finishes.)
        this.syncing = true;
        window.location = '/';
      }
    },

  }
});
