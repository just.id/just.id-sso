parasails.registerPage('sso-login', {
  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
  data: {
    // Main syncing/loading state for this page.
    syncing: false,

    // Form data
    formData: {
      sso: true,
      rememberMe: true,
    },

    // For tracking client-side validation errors in our form.
    // > Has property set to `true` for each invalid property in `formData`.
    formErrors: { /* … */ },

    // A set of validation rules for our form.
    // > The form will not be submitted if these are invalid.
    formRules: {
      emailAddress: { required: true, isEmail: true },
      password: { required: true },
    },

    // Server error state for the form
    cloudError: '',

    serviceName: '',
    returnUrl: '',
  },

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  beforeMount: function() {

  },
  mounted: async function() {

    if (!this.serviceName){
      let origin = new URL(this.returnUrl);
      switch(origin.hostname){
        case 'bitdocker.io':
          this.serviceName = 'BitDocker';
          break;
        case 'bitdocker':
          this.serviceName = 'BitDocker';
          break;
        default:
          this.serviceName = origin.hostname;
      }
    }
    // console.log(this.returnUrl);
  },

  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  methods: {
    submittedForm: async function(res) {
      // Redirect to the logged-in dashboard on success.
      // > (Note that we re-enable the syncing state here.  This is on purpose--
      // > to make sure the spinner stays there until the page navigation finishes.)
      this.syncing = true;

      if (res.token && this.returnUrl){

        let redirectUrl =  this.returnUrl + '?token=' + encodeURIComponent(res.token);
        window.location = redirectUrl;
      } else {
        window.location = '/';
      }

    },

    logout: async function() {
      await io.socket.get('/api/v1/auth/logout', (res, jwres) => {
        if (jwres.statusCode < 400){
          window.location.reload();
        } else {
          console.log('Logout failed.');
          return;
        }
      });
    }
  }
});
